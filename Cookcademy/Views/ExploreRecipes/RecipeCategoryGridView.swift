//
//  RecipeCategoryGridView.swift
//  Cookcademy
//
//  Created by Curtis Turk on 10/07/2023.
//

import SwiftUI

struct RecipeCategoryGridView: View {
//    private var recipeData = RecipeData()
    
    var body: some View {
        let columns = [GridItem(),GridItem()]
        
        ScrollView {
                LazyVGrid(columns:columns , content: {
                    ForEach(MainInformation.Category.allCases, id: \.self) { category in
                        NavigationLink(destination: RecipeListView(viewStyle:.singleCategory(category)), label: {CategoryView(category: category)})
                    }
                }).navigationTitle("Categories")
                
                NavigationLink("All Recipes") {
                    RecipeListView(viewStyle: .allRecipes)
//                        .environmentObject(recipeData)
                }.font(.title)
        }
    }
}

struct CategoryView: View {
    let category: MainInformation.Category
    
    var body: some View {
        ZStack{
            Image(category.rawValue).resizable().aspectRatio(contentMode: .fit).opacity(0.35)
            Text(category.rawValue).font(.title)
        }
    }
}

struct RecipeCategoryGridView_Previews: PreviewProvider {
  static var previews: some View {
      NavigationView{
          RecipeCategoryGridView()
      }
  }
}
