//
//  ModifyIngredientView.swift
//  Cookcademy
//
//  Created by Curtis Turk on 12/07/2023.
//

import SwiftUI

struct ModifyIngredientView: ModifyRecipeComponentView {
    
    @Binding var ingredient: Ingredient
    
    let decimalFormatter: NumberFormatter = {
           let formatter = NumberFormatter()
           formatter.numberStyle = .decimal
           return formatter
    }()
    
    let createAction: ((Ingredient) -> Void)
    
    init(component: Binding<Ingredient>, createAction: @escaping (Ingredient) -> Void) {
        self._ingredient = component
        self.createAction = createAction
    }
    
    @Environment(\.presentationMode) private var mode
    
    var body: some View {
        VStack{
            Form {
                TextField("Ingredient Name", text: $ingredient.name)
                Stepper(value: $ingredient.quantity, in: 0...100, step: 0.5){
                    HStack{
                        Text("Quantity:")
                        TextField("Quantity",
                                  value: $ingredient.quantity,
                                  formatter: decimalFormatter
                        ).keyboardType(.numbersAndPunctuation)
                    }
                }
                Picker(selection: $ingredient.unit, label:
                    HStack {
                        Text(ingredient.unit.rawValue)
                    }
                ){
                    ForEach(Ingredient.Unit.allCases, id: \.self) { unit in
                    Text(unit.rawValue)
                    }}.pickerStyle(MenuPickerStyle())
                HStack{
                    Spacer()
                    Button("Save",action: {
                        createAction(ingredient)
                        mode.wrappedValue.dismiss()
                    })
                    Spacer()
                }
            }
        }
    }
}

struct ModifyIngredientView_Previews: PreviewProvider {
    
    @State static var emptyIngredient = Ingredient()
    
    static var previews: some View {
        NavigationView {
            ModifyIngredientView(component: $emptyIngredient, createAction: { ingredient in
                print(ingredient)
              })
        }
    }
}


