//
//  RecipeData.swift
//  Cookcademy
//
//  Created by Curtis Turk on 06/07/2023.
//

import Foundation

class RecipeData: ObservableObject {
    
    @Published var recipes = Recipe.testRecipes
    
    func recipes(for category: MainInformation.Category?) -> [Recipe] {
        guard category != nil else { return recipes }
                
        return recipes.filter {
            $0.mainInformation.category == category
        }
    }
    
    func add(recipe: Recipe) {
        if recipe.isValid {
            recipes.append(recipe)
            saveRecipes()
        }
    }
    
    func index(of recipe: Recipe) -> Int? {
        for i in recipes.indices {
            if recipes[i].id == recipe.id {
                return i
            }
        }
        return nil
    }
    
    var favoriteRecipes: [Recipe] {
        recipes.filter{ $0.isFavorite }
    }
    
    private var recipesFileURL: URL {
      do {
        let documentsDirectory = try FileManager.default.url(for: .documentDirectory,
                                                             in: .userDomainMask,
                                                             appropriateFor: nil,
                                                             create: true)
        return documentsDirectory.appendingPathComponent("recipeData")
      }
      catch {
        fatalError("An error occurred while getting the url: \(error)")
      }
    }
    
    func saveRecipes() {
        do {
          let encodedData = try JSONEncoder().encode(recipes)
          try encodedData.write(to: recipesFileURL)
        }
        catch {
          fatalError("An error occurred while saving recipes: \(error)")
        }
      }
    
    
    func loadRecipes() {
      guard let data = try? Data(contentsOf: recipesFileURL) else { return }
      do {
        let savedRecipes = try JSONDecoder().decode([Recipe].self, from: data)
        recipes = savedRecipes
      }
      catch {
        fatalError("An error occurred while loading recipes: \(error)")
      }
    }
}
