//
//  ModifyComponentsView.swift
//  Cookcademy
//
//  Created by Curtis Turk on 12/07/2023.
//

import SwiftUI

protocol RecipeComponent: CustomStringConvertible, Codable{
    init()
    
    static func singularName() -> String
    static func pluralName() -> String
}

extension RecipeComponent {
    static func singularName() -> String {
        String(describing: self).lowercased()
    }
    static func pluralName() -> String {
        self.singularName() + "s"
    }
}

protocol ModifyRecipeComponentView: View {
    associatedtype Component
    
    init(component: Binding<Component>, createAction: @escaping (Component) -> Void)
}

struct ModifyRecipeComponentsView<Component: RecipeComponent, DestinationView: ModifyRecipeComponentView>: View where DestinationView.Component == Component {
    
    @Binding var components: [Component]
    @State private var newComponent = Component()
    
    var body: some View {
        
        VStack{
            let addComponentView = DestinationView(component: $newComponent) { component in
                components.append(component)
                newComponent = Component()
            }.navigationTitle("Add \(Component.singularName().capitalized)")
            
            if components.isEmpty{
                NavigationLink("Add the first \(Component.singularName())", destination: addComponentView)
                Spacer()
            }else {
                HStack{
                    Text(Component.pluralName().capitalized).font(.title).padding()
                    Spacer()
                    EditButton()
                        .padding()
                }
                List {
                    ForEach(components.indices, id: \.self) { index in
                        let component = components[index]
                        let editComponentView = DestinationView(component: $components[index]) { _ in return }
                            .navigationTitle("Edit \(Component.singularName().capitalized)")
                        NavigationLink(component.description, destination: editComponentView)
                    }
                    .onDelete { components.remove(atOffsets: $0) }
                    .onMove { indices, newOffet in components.move(fromOffsets: indices, toOffset: newOffet) }
                    
                    NavigationLink("Add another \(Component.singularName())",
                                   destination: addComponentView)
                        .buttonStyle(PlainButtonStyle())
                        
                }

            }
        }
    }
}

struct ModifyRecipeComponentsView_Previews: PreviewProvider {
    static var previews: some View {
//        ModifyComponentsView()
        Text("")
    }
}
