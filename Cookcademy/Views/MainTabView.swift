//
//  MainTabView.swift
//  Cookcademy
//
//  Created by Curtis Turk on 14/07/2023.
//

import SwiftUI

struct MainTabView: View {
    @StateObject var recipeData = RecipeData()
    
    var body: some View {
            TabView {
                NavigationView{
                    RecipeListView( viewStyle: .allRecipes)
                }
                .tabItem{
                    Label("All", systemImage: "list.dash")
                }
                
                NavigationView{
                    RecipeCategoryGridView()
                }
                .tabItem {
                    Label("Categories", systemImage: "square.grid.2x2")
                }
                
                NavigationView{
                    RecipeListView(viewStyle: .favorites)
                }
                .tabItem{
                    Label("Favorites", systemImage: "heart.fill")
                }
                SettingsView().tabItem { Label("Settings", systemImage: "gear")}
                
            }.environmentObject(recipeData).onAppear{
                recipeData.loadRecipes()
            }
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
