//
//  ModifyDirectionView.swift
//  Cookcademy
//
//  Created by Curtis Turk on 12/07/2023.
//

import SwiftUI

struct ModifyDirectionView: ModifyRecipeComponentView {
    @Binding var direction: Direction
    let createAction: (Direction) -> Void
    
    init(component: Binding<Direction>, createAction: @escaping (Direction) -> Void) {
        self._direction = component
        self.createAction = createAction
    }
    
    @Environment(\.presentationMode) private var mode
    
    var body: some View {
        Form{
            TextField("Direction Description", text: $direction.description)
            Toggle("Optional", isOn: $direction.isOptional)
            HStack {
                Spacer()
                Button("Save") {
                  createAction(direction)
                  mode.wrappedValue.dismiss()
                }
                Spacer()
            }
        }
    }
}

struct ModifyDirectionView_Previews: PreviewProvider {
    @State static var emptyDirection = Direction()
    
    static var previews: some View {
        NavigationView {
            ModifyDirectionView(component: $emptyDirection, createAction: {_ in return
            })
        }
    }
}
