//
//  ModifyRecipeView.swift
//  Cookcademy
//
//  Created by Curtis Turk on 11/07/2023.
//

import SwiftUI

struct ModifyRecipeView: View {
    @Binding var recipe: Recipe
    @State private var selection = Selection.mainInformation
    
    var body: some View {
        VStack {
            Picker("Select recipe component", selection: $selection){
                Text("Main info").tag(Selection.mainInformation)
                Text("Ingredients").tag(Selection.ingredients)
                Text("Directions").tag(Selection.directions)
            }.pickerStyle(SegmentedPickerStyle()).padding()
            
            switch selection {
                case .mainInformation:
                    ModifyMainInformationView(mainInformation: $recipe.mainInformation)
                case .ingredients:
                    ModifyRecipeComponentsView<Ingredient, ModifyIngredientView>(components: $recipe.ingredients)
                case .directions:
                    ModifyRecipeComponentsView<Direction, ModifyDirectionView>(components: $recipe.directions)
            }
            Spacer()
        }
    }
    
    enum Selection {
        case mainInformation
        case ingredients
        case directions
    }
}

struct ModifyRecipeView_Previews: PreviewProvider {
    @State static var recipe = Recipe()
    static var previews: some View {
        ModifyRecipeView(recipe: $recipe)
    }
}
