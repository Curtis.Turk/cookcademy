//
//  ModifyIngredientsView.swift
//  Cookcademy
//
//  Created by Curtis Turk on 12/07/2023.
//

import SwiftUI

struct ModifyIngredientsView: View {
    @Binding var ingredients: [Ingredient]
    @State var newIngredient = Ingredient()
    
    var body: some View {
//        NavigationView{
            VStack{
                if ingredients.isEmpty{
                    NavigationLink("Add an ingredient", destination:
                                    ModifyIngredientView(component: $newIngredient, createAction: { ingredient in
                        ingredients.append(ingredient)
                        newIngredient = Ingredient()
                    }))
                    Spacer()
                }else {
                    List(ingredients.indices, id: \.self) { index in
                        let ingredient = ingredients[index]
                        Text(ingredient.description)
                    }
                    NavigationLink("Add another ingredient", destination:
                                    ModifyIngredientView(component: $newIngredient,createAction: { ingredient in
                        ingredients.append(ingredient)
                        newIngredient = Ingredient()
                    })).buttonStyle(PlainButtonStyle())
                    Spacer()
                }
            }
//        }
    }
}

struct ModifyIngredientsView_Previews: PreviewProvider {
    
    @State static var emptyIngredients = [Ingredient]()
    @State static var recipe = Recipe.testRecipes[1]
    
    static var previews: some View {
        NavigationView {
          ModifyRecipeComponentsView<Ingredient, ModifyIngredientView>(components: $emptyIngredients)
        }
        NavigationView {
          ModifyRecipeComponentsView<Ingredient, ModifyIngredientView>(components: $recipe.ingredients)
        }
      }
}
