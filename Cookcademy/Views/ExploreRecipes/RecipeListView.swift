//
//  ContentView.swift
//  Cookcademy
//
//  Created by Curtis Turk on 06/07/2023.
//

import SwiftUI

struct RecipeListView: View {
    
    @EnvironmentObject private var recipeData: RecipeData
    
    @AppStorage("listBackgroundColor") private var listBackgroundColor = AppColor.background
    private let listTextColor = AppColor.foreground
    
    var category: MainInformation.Category?
    
    @State private var isPresenting = false
    @State private var newRecipe = Recipe()
    
    let viewStyle: ViewStyle
    
    var body: some View {
        List {
            ForEach(recipes) { recipe in
                NavigationLink(recipe.mainInformation.name) {
                    RecipeDetailView(recipe: binding(for: recipe))
                }
            }.listRowBackground(listBackgroundColor).foregroundColor(listTextColor)
        }.navigationTitle(navigationTitle)
        .toolbar(content: {
            ToolbarItem {
                Button(action: {
                    isPresenting = true
                }, label: {
                    Image(systemName: "plus")
                })
            }
        })
        .sheet(isPresented: $isPresenting, content: {
            NavigationView{
                ModifyRecipeView(recipe: $newRecipe).navigationTitle("Add a New Recipe").toolbar(content: {
                    ToolbarItem(placement: .cancellationAction) {
                        Button("Dismiss"){
                            isPresenting = false
                        }
                    }
                    ToolbarItem(placement: .confirmationAction){
                        if newRecipe.isValid {
                            Button("Add"){
                                if case .favorites = viewStyle {
                                    newRecipe.isFavorite = true
                                }
                                recipeData.add(recipe: newRecipe)
                                isPresenting = false
                            }
                        }
                    }
                })
            }
        })
    }
}

extension RecipeListView {

    enum ViewStyle {
        case favorites
        case allRecipes
        case singleCategory(MainInformation.Category)
    }
    
    private var recipes: [Recipe] {
        switch viewStyle {
            case let .singleCategory(category):
                return recipeData.recipes(for: category)
            case .favorites:
                return recipeData.favoriteRecipes
            case .allRecipes:
                return recipeData.recipes
        }
    }
    
    var navigationTitle: String {
        switch viewStyle {
            case let .singleCategory(category):
                return "\(category.rawValue) Recipes"
            case .favorites:
                return "Favorite Recipes"
            case .allRecipes:
                return "All Recipes"
        }
    }
    
    func binding(for recipe: Recipe) -> Binding<Recipe> {
        guard let index = recipeData.index(of: recipe) else {
          fatalError("Recipe not found")
        }
        return $recipeData.recipes[index]
    }
}

struct RecipeListView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            RecipeListView(viewStyle: .singleCategory(.dessert)).environmentObject(RecipeData())
        }
        NavigationView {
            RecipeListView(viewStyle: .favorites).environmentObject(RecipeData())
        }
        NavigationView{
        RecipeListView(viewStyle: .allRecipes).environmentObject(RecipeData())
        }
    }
}
